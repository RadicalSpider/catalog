<div class="card">
  <img class="card-img-top" src="{{ asset('images/' . $product->image) }}" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">{{ $product->name }}</h5>
    <p class="card-text">{{ $product->description }}</p>
    <a href="#" class="btn btn-primary">Ver detalle.</a>
  </div>
</div>